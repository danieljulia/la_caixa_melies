"use strict";

var lang;


var mouseX,mouseY;

$(document).ready(function(){
  $(document).on('mousemove',function(evt){
    mouseX = evt.pageX - $('.main-content').offset().left;
    mouseY = evt.pageY - $('.main-content').offset().top;
    if(app.current==10){
      app.casaRoll();

    }
    debug(mouseX+" "+mouseY);
  });
});


function App() {
  var hashlang=getHashValue("lang");
  if(!hashlang){
    if(config.lang!=undefined) lang=config.lang;
    else lang="ca";
  }
  else lang=hashlang;



  if(config.debug){
    $('#debug').removeClass('hidden');

  }

  this.current = 0;
  this.last = -1;
  this.init();
  this.started = false;

  var t=this;

  if(lang=="es"){
    $('.button.seguent').attr('src','images/botons/boto_seguent_es.png');
    $('#logo').attr('src','images/logo-recurs-educatiu-es.png');


  }

  $('body').addClass(lang);
  setInterval(function(){t.update()},1000);
}



App.prototype.update = function() {
    var that=this;
    if($('.fase'+this.current+" .dimoni").length){

      if(Math.random()<0.5){
        $('.fase'+this.current+" .dimoni img").attr("src","images/estudi/dimoni_2.png");
        setTimeout(function(){

          $('.fase'+that.current+" .dimoni img").attr("src","images/estudi/dimoni_1.png");
        },100);
      }
    }
}


App.prototype.init = function() {

  //carregar texts
  $("[data-label]").each(function() {
    var label = $(this).data("label");
    if (labels[label + "_" + lang] != undefined) {
      $(this).html(labels[label + "_" + lang]);
    } else {
      console.log("label no trobat: "+label + "_" + lang);
    //  $(this).html(label + "_" + lang);
    }
  });
  this.interface();
  this.updateFase();
};

App.prototype.go = function(c,force) {

  if(force){
    this.current=-1;
    this.last=-1;

  }
  if (c == this.current) return;
  this.current = c;
  if (this.current < 0) this.current = 0;
  //todo limit final?
  audioStopAll();
  this.updateFase();
};
App.prototype.interface = function() {
  var that = this;

};

App.prototype.addBack = function(back) {
  var that = this;

  if ($(".fase" + this.current + " .tornar").length) return;

  $(".fase" + this.current).prepend(
    '<div data-back="' +
      back +
      '" class="button tornar"><img src="images/tornar_'+lang+'.png"></div>'
  );
  $(".tornar")
    .off("click")
    .on("click", function(ev) {
      that.current = $(this).data("back");

      that.updateFase();
      ev.preventDefault();
    });
};

App.prototype.updateFaseEx = function() {
  var that = this;
  var t = 1000;


  if (this.last==0 && this.current == 1) {

    $(".fase").hide();
        $(".fase1").show();
        this.current=1;
        this.initStatus();
  } else {

    $(".fase").hide();
    this.cache();
    //  $('.fase').fadeOut(t,function(){

    $(".fase" + that.current).fadeIn(t, function() {
      that.initStatus();
    });

  }


};

App.prototype.updateFase = function() {
  var that = this;

  audioStopAll();

  //todo millorar
  $(".anim-left").hide();
  $(".anim-right").hide();
  $(".anim-top").hide();
mytext.hide();
  if (this.last != -1 && this.last!=0) {

    $(".fase" + this.last).fadeOut(500, function() {
      that.updateFaseEx();
    });
  } else {
    that.updateFaseEx();
  }
};

App.prototype.setBehaviours = function() {};

App.prototype.fase1anim = function(cla) {
  var that = this;

  var top=parseInt($('.fase1 .titol').css('top'));
  if(top>40){
      anim_to_top(".fase1 .titol");
  }

  $(".melies").attr("src", "images/intro/melies4.png");
  $(cla).addClass("button");

  setTimeout(function() {
    $(".melies").attr("src", "images/intro/melies5.png");

    $(cla).velocity({ opacity: 1 }, { duration: 1000 });
  }, 1000);

  setTimeout(function() {
    if (cla == ".globus") that.fase1anim(".retrat");
    if (cla == ".retrat") that.fase1anim(".barret");
  }, 2000);

}


App.prototype.global = function(cla) {
  var that = this;

    $("#myvideo").attr("src", "");

$(".more-info").off('click').on("click", function() {
  $('.fase'+that.current+' .text-info').toggleClass("hidden");

  if($('.fase'+that.current+' .text-info').hasClass("hidden")){
      $('.fase .text-info').css('z-index','');
  }else{
    $('.fase .text-info').css('z-index','7777');
  }

});

  //$(".globus img").velocity("stop", true);

  if(this.current==1){
    $(".fase1 .globus img").velocity("stop", true);

    //if(!$(".fase1 .globus img").is('.velocity-animating')){
      $(".fase1 .globus img").velocity(
        { top: ["191px", "140px"] },
        {
          duration: 2000,
          loop: 1000
        }
      );
  //}
  }

  if(this.current==2){
    $(".fase2 .globus img").velocity("stop", true);
    //if(!$(".fase2 .globus img").is('.velocity-animating')){
      $(".fase2 .globus img").velocity(
        { top: ["191px", "140px"] },
        {
          duration: 2000,
          loop: 1000
        }
      );
  //  }
  }


  $(".anim-left").each(function() {
    var left = $(this).css("left");
    $(this).velocity({ left: [left, "-500px"] }, { duration: 500 });
    $(this).show();
  });

  $(".anim-right").each(function() {
    var left = $(this).css("left");
    $(this).velocity({ left: [left, "1000px"] }, { duration: 500 });
    $(this).show();
  });





  $(".anim-top").each(function() {
    var html=$(this).html();
    html=html.replace("<p>","");
  html=html.replace("</p>","");
  $(this).html(html);

    var top = $(this).css("top");
    $(this).velocity({ top: [top, "-100px"] }, { duration: 500 });
    $(this).show();
  });

  $("[data-go]").each(function() {
    $(this).off("click").on("click", function() {
      var num = $(this).data("go");
      that.go(num);
    });
  });
};

App.prototype.updateTruc = function() {
  audioStopAll();


  var that = this;
  switch (this.currenttruc) {
    case 1:
      playAudio("teatre/aplaudiments_curt");
      $(".carpeta").on("click", function() {
        playAudio("teatre/carpeta");
        $(".carpeta img").attr("src", "images/trucs/truc1_carpeta_oberta.png");
        setTimeout(function() {
          $(".carpeta img").attr("src", "images/trucs/truc1_carpeta.png");
          if (that.truc1.pas == 0) {
            $(".truc1 .objectes").append(
              "<img class='objecte candelabre' src='images/trucs/truc1_candelabre.png'>"
            );

            $('.candelabre').css({left:'550px',top:'450px',rotateZ:'0deg'});
            moveTo('.candelabre',370,260,'-=180',function(){
              moveTo('.candelabre',314,394,'0');
            });


            that.truc1.pas = 1;
            return;
          }
          if (that.truc1.pas == 1) {
            $(".truc1 .objectes").append(
              "<img class='objecte gabia' src='images/trucs/truc1_gabia.png'>"
            );


            $('.gabia').css({left:'500px',top:'450px',rotateZ:'180deg'});
            moveTo('.gabia',560,260,-180,function(){
              moveTo('.gabia',670,391,-0);
            });

            /*
            $(".gabia").velocity(
              {
                left: ["670px", "500px"],
                top: ["391px", "450px"],
                  rotateZ:['0deg','180deg']
              //  transform: ["rotateZ(0deg)", "rotateZ(180deg)"]
              },
              { duration: 500 }
            );*/
            that.truc1.pas = 2;
            return;
          }
          if (that.truc1.pas == 2) {
            $(".truc1 .objectes").append(
              "<img class='objecte senyora' src='images/trucs/truc1_senyora.png'>"
            );

            $(".senyora").velocity(
              {
                left: ["460px", "520px"],
                top: ["325px", "335px"],
                opacity: [1, 0.2],
                  rotateZ:['-10deg','20deg']
                //transform: ["rotateZ(-10deg)", "rotateZ(20deg)"]
              },
              { duration: 100 }
            );

            that.truc1.pas = 3;
            setTimeout(function() {
              playAudio("teatre/oh_aplaudiment");
                $(".carpeta").off("click");
            }, 500);
          }
        }, 500);
      });

      break;
    case 2:
      playAudio("teatre/blablabla", null, true);

      blah();
      function blah() {
        var num = Math.floor(Math.random() * 2 + 1);

        $(".fase4 .cap img").attr("src", "images/trucs/truc2_cap" + num + ".png");
        setTimeout(function() {
          blah();
        }, Math.floor(Math.random() * 200));
      }

      $(".espasa img").velocity(
        {
          //transform: ["rotate(30deg)", "rotate(-30deg)"]
            rotateZ:['30deg','-30deg']

        },
        {
          duration: 1000,
          loop: 1000
        }
      );

      $(".espasa").on("click", function() {
        setTimeout(function(){
            playAudio('teatre/sable_2');
        },500);

        //$(".espasa").velocity("stop", true);
        $(".espasa").off("click");

        $(this).velocity(
          {
            //transform: ["rotateZ(360deg)", "rotateZ(0deg)"]
              rotateZ:['360deg','0deg']
          },
          {
            duration: 1000
          }
        );
        setTimeout(function() {
          $(".truc2 .cap").velocity(
            {
              top: "250px"
            },
            {
              duration: 100,
              complete: function() {
                $(".truc2 .cap").velocity(
                  {
                    top: "800px",
                    left: "-200px",
                    //transform: ["rotateZ(-180deg)", "rotateZ(0deg)"]
                      rotateZ:['-180deg','0deg']

                  },
                  {
                    duration: 800,
                    complete: function() {
                      $(".fase4 .cap").css({
                        left: "650px",
                        top: "360px",
                        transform: "rotateZ(0deg)"
                          //rotateZ:'0deg'

                      });
                      $(".armari img").attr(
                        "src",
                        "images/trucs/truc2_armari_obert.png"
                      );
                    }
                  }
                );
              }
            }
          );
        }, 300);
      });

      break;
    case 3:
      this.truc3.num = 1;
            playAudio("teatre/aplaudiments_curt");
      $(".truc3 .proj").on("click", function() {
      //  playAudio("teatre/llanterna_mecanisme");

        that.truc3.num++;

        if (that.truc3.num > 6) {
          return;
        }

        if (that.truc3.num == 2) {

            //playAudio("llum");
        }

        if (that.truc3.num == 6) {
          playAudio("teatre/oh_aplaudiment");
        }

        $(".proj img").attr(
          "src",
          "images/trucs/truc3_proj" + that.truc3.num + ".png"
        );

        if (that.truc3.num > 2) {
          $(".foto img").attr(
            "src",
            "images/trucs/truc3_foto" + (that.truc3.num - 2) + ".png"
          );
          $(".foto").removeClass("hidden");
        }
        //ª<1<if(that.truc3.num)
      });

      break;
  }
};


App.prototype.cache = function() {

  if(!$('.fase'+this.current).data('html')){
    $('.fase'+this.current).data('html',$('.fase'+this.current).html());
  }else{
    $('.fase'+this.current).html($('.fase'+this.current).data('html'));
  }
}


App.prototype.casaRoll = function() {


if ($('.casa:hover').length == 0)  return; //todo arreglar per touch

var punts={
  2:[708,355],3:[509,356],4:[327,261],5:[413,165],6:[258,300],7:[704,345],8:[158,288]
}


  var punt=-1;

  var min=1000000;
  for(var i=2;i<=8;i++){
    var dx=punts[i][0]-mouseX;
    var dy=punts[i][1]-mouseY;
    var d=Math.sqrt(dx*dx+dy*dy);
    if(d<min){
      min=d;
      punt=i;
    }
  }

  if(punt!=this.lastpunt && d<500){
    this.lastpunt=punt;
    playAudio("click");
    $(".casa img").attr("src", "images/casa/casa_" + punt + ".png");
    var txt=labels["estudi_"+punt+"_"+lang];
      mytext.setCurrent(punt-2);
      //$('.fase10 .content').html(txt);

    this.lastpunt=punt;
  }

}

App.prototype.initStatus = function() {
  var that = this;

  if (this.last == this.current) return;
  this.last = this.current;

  this.setBehaviours();
  this.global();

  if (this.current > 1 && this.current!=16) this.started = true;

  switch (this.current) {
    case 0:

      $(".melies").velocity("stop", true);
      $(".melies").velocity({ opacity: [1, 0] }, { duration: 2000 });
      $(".terra").velocity({ opacity: [1, 0] }, { duration: 2000 });


      $(".melies").on("click", function() {
        playAudio("click");
        that.go(1);
      });

      /*$('.melies').on('mouseover',function(){
        that.go(1);
      });
*/

      //començar sol
      setTimeout(function(){
        return;//no s'engega sol
        if(that.current==0){
              that.automatic=true;
          that.go(1);


        }
      },5000);

      break;

    case 1:
        setTimeout(function(){
          playAudio("inici", true);
        },2000);


      if (this.started) {
        $(".fase1 .titol").hide();
        $(".melies").attr("src", "images/intro/melies3.png");
        $('.globus').addClass("button");
        $('.barret').addClass("button");
        $('.retrat').addClass("button");
          $(".fase1 .button").removeClass("hidden");
          animSpriteLoop('.fase1 .barret img',['images/barret1.png','images/barret2.png','images/barret3.png']);
            animSpriteLoop('.fase1 .retrat img',['images/bigotis_1.png','images/bigotis_2.png','images/bigotis_3.png']);
        return;
      }

      animSpriteLoop('.fase1 .retrat img',['images/bigotis_1.png','images/bigotis_2.png','images/bigotis_3.png']);
      animSpriteLoop('.fase1 .barret img',['images/barret1.png','images/barret2.png','images/barret3.png']);


      $(".melies").attr("src", "images/intro/melies2.png");
      setTimeout(function() {
        $(".melies").attr("src", "images/intro/melies3.png");
      }, 500);

      setTimeout(function() {
        playAudio("Campanetes");
        anim_from_top(".fase1 .titol");
      }, 500);


      if(this.automatic){
        setTimeout(function(){
            that.fase1anim(".globus");
        },5000);


      }
      if (!this.started) {

        $(".melies").on("click", function() {
            playAudio("click");
          $(".melies").off("click");
          that.fase1anim(".globus");
          //anim_to_top(".fase1 .titol");
        });
    }
      break;

    case 2:
      that.addBack(1);


      playAudio('paris',true);

      mytext.setSection("globus");
      mytext.init();

      $('.fase2 .globus').show();

      $(".llumeta").each(function(){

        var d=500+Math.floor(Math.random()*800);

        $(this).velocity(
          { opacity: [1, 0] },
          {
            duration: d,
            loop: 1000
          }
        );

      });


      $(".llumeta").on("mouseover", function() {
        playAudio("campaneta");
      });
      $(".llumeta")
        .off("click")
        .on("click", function(ev) {
          var num = $(this).data("num");

          mytext.setCurrent(num);
          ev.preventDefault();
        });

      playAudio("acordio");

      break;
    case 3:
      that.addBack(1);

      animSpriteLoop('.fase3 .retrat img',['images/bigotis_1.png','images/bigotis_2.png','images/bigotis_3.png']);


        playAudio("bio",true);
      mytext.setSection("biografia");
      mytext.init();
      $(".barra li")
        .off("click")
        .on("click", function() {
          var num = $(this).data("num");
          $('.barra li').removeClass("selected");
          $(this).addClass("selected");
          mytext.setCurrent(num);
        });
      break;

    case 4:
      $('.fase4').data("infoopened",false);
      playAudio('magia_2',true);
      playAudio('teatre/murmuri');
      if(this.repeattruc!=undefined){
        this.currenttruc=this.repeattruc;
        this.repeattruc=undefined;
      }else{
        this.currenttruc = 0;

      }
      $(".fase4 .botons .seguent").on("click", function() {
        $(".fase4 .text").hide();
        that.cortina.do(3);

        nextTruc();
      });

      $(".fase4 .text").on("click",function(){
        infoTanca();
      });


      function infoObre(){
        var c=that.currenttruc ;
        if(c==0) c=1;
        var txt =labels["joc_" + c+ "_" + lang];

        $(".fase4 .text").text(txt);
        that.cortina.do(2, function() {
          $(".fase4 .text").show();//removeClass("hidden");
        });
        $('.fase4').data("infoopened", true);

      }

      function infoTanca(){
        $(".fase4 .text").hide();//addClass("hidden");
        that.cortina.do(3, function() {});
        $('.fase4').data("infoopened", false);

      }

      $(".fase4 .botons .joc-info").on("click", function() {
        if ($('.fase4').data("infoopened")) {
          infoTanca();
        } else {
          infoObre();
        }
      });

      $(".fase4 .botons .reload").on("click", function() {
        that.current=1;
        that.last=-1;
        that.repeattruc=that.currenttruc-1;
        that.go(4);


      });

      function nextTruc() {
        that.cortina.obre(function() {
          that.currenttruc++;
        if(that.currenttruc==0) that.currenttruc=1;

          if (that.currenttruc == 4) {
            //s'ha acabat

            that.go(5);
            return;
          }
          $(".truc").hide();
          $(".trucs .truc" + that.currenttruc).show();
          that.updateTruc();
        });
      }

      this.cortina = new Cortina();

      $(".truc").hide();

      $(".fase4 .cortina-superior .escut").velocity(
        {
          //transform: ["rotateZ(-5deg)", "rotateZ(0deg)"],
          rotateZ:['-5deg','0deg'],
          scale: [1.02, 1]
        },
        {
          duration: 500,
          loop: 5
        }
      );


      $(".fase4 .escut").on("click", function() {
        $('.fase4 .botons img').removeClass("hidden");
        nextTruc();
      });

      that.addBack(1);
      this.cortina_tancada = true;

      initJocs();
      function initJocs() {
        that.truc1 = { pas: 0 };
        that.truc3 = { num: 1 };
      }

      break;

    case 5:
      //estudi
      that.addBack(1);


      playAudio("estudi",true);


      $(".fase5 .button").on("mouseout", function(ev) {
        ev.stopPropagation();
        var tooltip=$(this).data('tooltip');

        setTimeout(function(){
          $('.tooltip.'+tooltip).fadeOut(1000);
        },1000);
      });

      /*
      $(".fase5 .button").on("mouseout", function(ev) {
        var tooltip=$(this).data('tooltip');
        //setTimeout(function(){
              $('.tooltip.'+tooltip).hide();
    //    },500);
      ev.stopPropagation();
    });*/

      $(".fase5 .cinematograf").on("click", function() {
        that.go(6);
      });

      $(".fase5 .cinematograf").on("mouseover", function() {
          playAudio("click_0");
        var tooltip=$(this).data('tooltip');
        $('.tooltip.'+tooltip).show();
        $(this).velocity(
          {
            //transform: ["rotateZ(-5deg)", "rotateZ(0deg)"],
              rotateZ:['-5deg','0deg'],

            scale: [1.2, 1.1]
          },
          { duration: 500 }
        );

      });

      $(".cofre").on("click", function() {
        that.go(9);
      });

      $(".cofre ").on("mouseover", function() {
          playAudio("click_0");
        var tooltip=$(this).data('tooltip');
        $('.tooltip.'+tooltip).show();
        $(this).find('img').attr("src", "images/estudi/estudi_cofre_obert.png");
        $(".cofre").css({ left: "292px", top: "414px" });
        //  playAudio("click");
      });

      $(".cofre ").on("mouseout", function() {
        $(this).find('img').attr("src", "images/estudi/estudi_cofre_tancat.png");
        $(".cofre").css({ left: "300px", top: "440px" });
      });

      $(".plano").on("click", function() {
        that.go(10);
      });

      $(".dimoni").on("click", function() {
        that.go(11);
      });

      $(".fase5 .plano.button").on("mouseover", function(ev){
          playAudio("click_0");
        var tooltip=$(this).data('tooltip');
        $('.tooltip.'+tooltip).show();
        ev.stopPropagation();
      });

      $(".fase5 .dimoni.button").on("mouseover", function(ev){
          playAudio("click_0");
        var tooltip=$(this).data('tooltip');
        $('.tooltip.'+tooltip).show();
        ev.stopPropagation();
      });

      break;

    case 6:
      that.addBack(5);

      $(".fase6 .cinematograf").on("click", function() {
        that.go(7);
      });
      $(".fase6 .cinematograf_retol").on("click", function() {
        that.go(8);
      });

      break;
    case 7:
      that.addBack(6);
      mytext.setSection("projector");

      over_transparent(".fase7 .interior", function() {
        mytext.setCurrent(0);
      });

      over_transparent(".fase7 .obturador", function() {
        mytext.setCurrent(1);
      });

      over_transparent(".fase7 .pelicula", function() {
        mytext.setCurrent(2);
      });

      $(".fase7 .manivela").on("mouseover click", function() {
          playAudio("manovella_loop");
        mytext.setCurrent(3);
        $(this).css({ transform: "rotateX(180deg)", top: "276px" });
        var that = this;
        setTimeout(function() {
          $(that).css({ transform: "rotateX(0deg)", top: "266px" });
        }, 500);
      });

      break;
    case 8:
      that.addBack(6);

      mytext.setSection("germans");
      mytext.setCurrent(0);

      break;
    case 9:
      that.addBack(5);

      $("#myvideo").attr("src", "");

      mytext.setSection("pelis");
      mytext.init();



      var current_peli=0,current_peli_url='';

        $('.fase9 .peli').on('mouseover',function(){
          $(this).css("transform","scale(1.1) rotate(5deg)")
        });

        $('.fase9 .peli').on('mouseout',function(){
          $(this).css("transform","scale(1.1) rotate(0deg)")
        });

      	$('.fase9 .peli').on('click',function(){
      			var id=$(this).data('id');
      			current_peli=(id);

      			var code=labels['peli_'+id+'_code'];
      			var url="https://www.youtube.com/embed/"+code+"?autoplay=1&rel=0&controls=0&showinfo=0&amp;wmode=transparent";
      			$('#myvideo').attr('src',url);
      			$('#myvideo').show();

      			current_peli_url=url;
      			//$('.fase9 .titol').text(labels['peli_'+id]);
      			mytext.setCurrent(id);
      			$('.fase9 .text-frame .content').hide();

      	});

      		$('.fase9 .peli-info').on('click',function(){

      			//mytext.setCurrent(id-1);
      			$('#myvideo').toggle();
      			$('.fase9 .text-frame .content').toggle();
      			if(!$('#myvideo').is(':visible')){
      				$('#myvideo').attr('src','');
      			}else{
      				$('#myvideo').attr('src',current_peli_url);
      			}
      		});




      break;

    case 10:
      that.addBack(5);

      mytext.setSection("casa");
      mytext.setCurrent(0);
          $(".casa img").attr("src", "images/casa/casa_" + 2 + ".png");


      break;
    case 11:
      that.addBack(5);


      break;

    case 12: //truc substitució

      that.addBack(11);


      setTimeout(function(){
          $('.fase12 .camara').velocity({scale:1.1},{duration:1000});
      },1000);



      function camroda(){
          if(that.current!=12) return;
          setTimeout(function(){
              if(that.current!=12) return;
            playAudio("camara");
            animSprite(".fase12 .camara img",
            "images/trucatges/truc_2_cam_right.png",
          "images/trucatges/truc_2_camb_right.png",20);
          },1200);

      }



      $(".fase12 .camara").off("click").on("click", function() {
        setTimeout(function(){camroda()},1000);

        anim_from_top(".cadira");
        $(this).off("click");

        truc1();
      });

      function truc1() {
        if(that.current!=12) return;
        setTimeout(function() {
          camroda();

          anim_from_top(".senyora");
          setTimeout(function() {
              if(that.current!=12) return;
              camroda();

            anim_to_top(".senyora");
            setTimeout(function() {
                if(that.current!=12) return;
              $(".fase12 .gran").hide();
              $(".fase12 .camara").hide();
              mostraPantalla();
            }, 5000);
          }, 3000);
        }, 4000);
      }

      function mostraPantalla() {
        if(that.current!=12) return;
        $(".fase12 .pantalla").removeClass("hidden");

        setTimeout(function() {
          $(".fase12 .pantalla img").attr(
            "src",
            "images/trucatges/truc1_pantalla_2.png"
          );
          if(that.current!=12) return;
          setTimeout(function() {
            $(".fase12 .pantalla img").attr(
              "src",
              "images/trucatges/truc1_pantalla_3.png"
            );
            setTimeout(function() {
              $(".fase12 .pantalla img").attr(
                "src",
                "images/trucatges/truc1_pantalla_2.png"
              );
              setTimeout(function(){
                that.go(12,true);
              },5000)

            }, 2000);
          }, 2000);
        }, 2000);
      }
      break;
    case 13: //joc  d'escala
      that.addBack(11);

      var pasos=[[408,321],[458,317],[524,311],[600,304]];
      var c=0;



      setTimeout(function(){
          $('.fase13 .camara').velocity({scale:0.8},{duration:1000});
      },1000);


      $('.fase13 .camara').on('click',function(){
          $('.fase13 .camara').off('click');
            $('.fase13 .carret').removeClass('hidden');
            $('.fase13 .carret').velocity({
              'margin-top':[0,-500]
            },{
              duration:1000,
              complete:function(){
                setTimeout(function(){
                    pas();
                },1000);


              }
            });


      });


      function pas(){
        if(that.current!=13) return;
        if(c>=pasos.length) return;

        $('.carret').velocity({ left:pasos[c][0]+'px',top: pasos[c][1]+'px' }, { duration: 500,
        complete:function(){
            playAudio("camara");
          animSprite(".fase13 .camara img",
          "images/trucatges/truc_2_cam.png",
        "images/trucatges/truc_2_camb.png",10);
        } });
        c++;


        if(c<pasos.length) setTimeout(pas,2000);
        else{
          setTimeout(function(){
            if(that.current!=13) return;
            fadeOut('.fase13 .anim1')
            //fadeIn('.fase13 .pantalla')
              $(".fase13 .pantalla").removeClass("hidden");
                      $(".fase13 .pantalla").removeClass("invisible");
                $(".fase13 .cap").removeClass("hidden");
            //$('.fase13 .cap').css('opacity',1);
            mostracap(1);
          },5000);
        }
      }

      function mostracap(size){
        if(that.current!=13) return;
        animSprite(".fase13 .cap img",
        "images/trucatges/truc_3_cap_3.png",
        "images/trucatges/truc_3_cap_4.png",5);
          $(".fase13 .cap img").css({transform:'scale('+size+')'});
          if(size<2.5){
            setTimeout(function(){
              mostracap(size+0.5);
            },1500);
          }else{
            setTimeout(function(){
              that.go(13,true);
            },1500);

          }
      }





      break;
    case 14:

      that.addBack(11);
      var c=0;

      function rodaCamara(){
        if(that.current!=14) return;
        setTimeout(function(){
          if(c==1)playAudio("camara_clecle");
          else playAudio("camara");
          c++;
              animSprite(".fase14 .camara img",
              "images/trucatges/truc_2_cam.png",
            "images/trucatges/truc_2_camb.png",10);
        },1000);

      }

      setTimeout(function(){
          $('.fase14 .camara').velocity({scale:0.8},{duration:1000});
      },1000);


      $('.fase14 .camara').on('mouseover',function(){
        $(this).velocity({scale:0.9},{duration:500});
      });
      $('.fase14 .camara').on('mouseout',function(){
        $(this).velocity({scale:0.8},{duration:500});
      });


      $('.fase14 .camara').on('click',function(){
        $('.fase14 .camara').off('click');

          $('.fase14 .camara').off('mouseover');

          anim_from_top('.fase14 .senyor1');

          rodaCamara();



          setTimeout(function(){
            if(that.current!=14) return;
            anim_to_top('.fase14 .senyor1');
            rodaCamara();
            setTimeout(function(){
                  if(that.current!=14) return;
              anim_from_top('.fase14 .senyor2');
              rodaCamara();

              setTimeout(function(){
                    if(that.current!=14) return;
                $('.fase14 .anim1').addClass('hidden');
                $('.fase14 .resultat').removeClass('hidden');
              /*  fadeOut('.fase14 .anim1',function(){
                  fadeIn('.fase14 .resultat');
                });*/
                setTimeout(function(){
                      if(that.current!=14) return;
                  that.go(14,true);
                },4000);

              },5000);

            },3000);
          },3000);
      });

      break;
    case 15:
      that.addBack(11);
      var current=1;
      updateCurrent();

      $('.fase15 .left').on('click',function(){
        current--;
        if(current<1) current=6;
        updateCurrent();
      });

      $('.fase15 .right').on('click',function(){
        current++;
        if(current>6) current=1;
        updateCurrent();
      });

      function updateCurrent(){

        //fadeOut('.fase15 .diapo',function(){
          $('.fase15 .diapo img').attr('src','images/trucatges/truc_4_diapo_'+current+'.png');
        //  fadeIn('.fase15 .diapo');
          //caption
        //});

        var txt=labels['art_' +current+"_" + lang];
        $('.fase15 .caption').html(txt);
        $('.fase15 .diapo').velocity({
          opacity:1
        },500);

      }

      break;

    case 16:
      that.addBack(1);
      var c_credits=1;

      var credits_pos_ca=[[50,50],[50,86],[50,76]];
      var credits_pos_es=[[50,50],[52,80],[56,74]];

      updateCredits();

      function updateCredits(){
        var cr=eval('credits_pos_'+lang);
        $('.credits img').velocity({
          opacity:0},
          {duration:500,
          complete:function(){
            $('.credits img').attr('src','images/credits/credits_'+c_credits+'_'+lang+'.png');
            $('.credits img').load(function(){
              $('.credits img').css({
                left:cr[c_credits-1][0]+'px',
                top:cr[c_credits-1][1]+'px',
                opacity:1
            });
            });

          }}
        );

        $('.credits img').removeClass("hidden");
        updateCreditsPaginacio()
      }

      function updateCreditsPaginacio(){
        $('.fase16 li').removeClass('selected');
        $('.fase16 .indx'+c_credits).addClass('selected');
      }

      $('.credits').on('click',function(){
        c_credits++;
        if(c_credits>3) c_credits=1;
        updateCredits();
      });

      $('.fase16 .paginacio li').on('click',function(){

          c_credits=$(this).data('indx');
          updateCredits();
      });

    break;
    default:
  }
};

var app;
var mytext;
$(document).ready(function() {
  //no se perquè no m'ho deixa posar al principi
  mytext = new Text();
  app = new App();
  $("body").keyup(function(e) {
    if (e.keyCode >= 48 && e.keyCode <= 57) {
      //0-9 only
      var num = e.keyCode - 48;
      app.go(num);
    }
  });
});
