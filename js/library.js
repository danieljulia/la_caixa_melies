/**
funcions generals
*/

function getHashValue(key) {
  var matches = location.hash.match(new RegExp(key+'=([^&]*)'));
  return matches ? matches[1] : null;
}


var audios=[];
var locutions_played=[];

function playAudio(name,has_lang,loop){
  if(!config.sound) return;
  if(has_lang!=undefined){
    if(locutions_played.indexOf(name)!=-1) return;
    locutions_played.push(name);
    name="resources/locutions/"+name+"_"+lang;

  }else{
    name='resources/sounds/'+name;
  }

  var audio = new Audio(name+'.mp3?');
  if(loop) audio.loop=true;
  audio.play();
  audios.push(audio);
}

function audioStopAll(){
  for(var i=0;i<audios.length;i++){
    audios[i].pause();
  }
}

function debug(txt){
  if(config.debug){
    $('#debug').text(txt);
  }
}
function setText(sel,label){
  $(sel).text(getText(label));
}

function getText(label){
  var txt=eval("labels_"+lang)[label];
  if(txt==undefined) return "undefined: "+label;
  return txt;
}


function moveTo(sel,left,top,rotation,cb){

  var par={
    left:left+'px',
    top: top+'px'};
  if(rotation!=undefined){
      par.rotateZ=rotation+"deg";
  }

  var par2={
    duration:500
  }
  if(cb!=undefined){
    par2.complete=cb;
  }



  $(sel).velocity(
  par,par2);

}

function animSpriteLoop(sel,ims){
  var c=0;
  doit();
  function doit(){
    c++;
    if(c==ims.length) c=0;
    $(sel).attr('src',ims[c]);
    setTimeout(function(){
      doit();
    },500);
  }
}

function animSprite(sel,im1,im2,times){

  $(sel).attr('src',im2);
  var c=0;
  doit();
  function doit(){

    if(c%2==0){
      $(sel).attr('src',im1);

    }else{
      $(sel).attr('src',im2);

    }
    setTimeout(function(){
      c++;
      if(c<times) doit();
    },100);
  }
}



function over_transparent(sel,cb){
  $(sel).on('mouseover click',function(){
      playAudio("click_camera");
    $(this).css({opacity:1});
    cb();
  });
  $(sel).on('mouseout',function(){
    $(this).css({opacity:0});
  });
}

function anim_from_top(sel){
  var top=$(sel).css('top');
  $(sel).css('top','-500px');
  $(sel).velocity({
    top:top
  },{
    duration:1000
  //  complete:cb()
  });
  $(sel).removeClass('hidden');
}

function anim_to_top(sel){
  $(sel).velocity({
    top:'-500px'
  },{
    duration:1000
  //  complete:cb()
  });

}

function fadeIn(sel,cb){
    $(sel).velocity("stop", true);
  var par={duration:500};
  if(cb!=undefined) par.complete=cb();

  $(sel).velocity({
    opacity:1
  },par);
}

function fadeOut(sel,cb){
      $(sel).velocity("stop", true);
  var par={duration:500};
  if(cb!=undefined) par.complete=cb();

  $(sel).velocity({
    opacity:0
  },par);
}


function Text(){

}

Text.prototype.hide=function(){

        $('.text-frame .paginacio').html('');
  $('.text-frame').hide();
}

Text.prototype.setSection=function(sec){

  this.section=sec;
  for(var i=0;i<textos.length;i++){
    if(textos[i].name==sec){
      this.contents=textos[i].data;
    }
  }
  this.current=0;
  this.started=true;
}


Text.prototype.reset=function(){
  $('.text-frame .titol').velocity("stop", true);
  $('.text-frame .content').velocity("stop", true);

}

Text.prototype.init=function(){
  this.update();
}

Text.prototype.setCurrent=function(c){
  if(c==this.current && !this.started) return;
  this.started=false;
  this.current=c;
  this.reset();
  var that=this;
  this.init();
}

Text.prototype.getTitle=function(){
  var t=eval("this.contents[this.current].title");
  if(t!=undefined){
    return t;
  }
  var t=eval("this.contents[this.current].title_"+lang);
  return t;

}

Text.prototype.update=function(){
      var that=this;
    $('.text-frame').fadeIn();
    if(this.contents[this.current].ico!=undefined){
      $('.text-frame .icona').velocity(
        { "opacity": 0 },
        {  duration: 500,
        complete:function(){
          $('.text-frame .icona img').attr('src',"images/"+that.contents[that.current].ico);
          $('.text-frame .icona').velocity(
            { "opacity": 1 },
            {  duration: 500} )
        }
      }
    );
    }

    if(this.contents[this.current].image!=undefined){
      $('.foto-frame').hide();
      $('.info img').attr('src',"images/"+this.contents[this.current].image);
      $('.info').removeClass('hidden');
    }else{
      $('.info').addClass('hidden');
    }

    if(this.contents[this.current].caption!=undefined){
      $('.info .caption').html(this.contents[this.current].caption);
    }

    var caption=eval("this.contents[this.current].caption_"+lang);
    if(caption!=undefined){
      $('.info .caption').html(caption);
    }

  if(this.contents[this.current].text_ca==undefined){
    //només hi ha titol
    //centrat
    $('.text-frame .titol').addClass("single");
    if(app.current==9){
      $('.text-frame .titol').css({top:'350px'});
    }else{
      $('.text-frame .titol').css({top:'200px'});
    }

        $('.text-frame .content').text('');
        var titol=this.getTitle();

        $('.text-frame .titol').text(titol);
        $('.text-frame .titol').show();
        /*
        $('.text-frame .titol').velocity(
          {top:['200px','-100px']},
          {duration:5000}
        );*/

    $('.text-frame .titol').velocity(
      { "margin-left": ['0px','+1500px'] },
      {  duration: 500 }
    );


  }else{
    $('.text-frame .titol').removeClass("single");
    $('.text-frame .content').css({opacity:0});
    var content=eval("this.contents[this.current].text_"+lang);
    if(content.constructor === Array){


      var num=content.length;

      var html="<ul>";
      for(var i=0;i<num;i++){
        html+="<li data-pag='"+(i)+"'>"+(i+1)+"</li>";
      }
      html+="</ul>";


      $('.text-frame .paginacio').html(html);


      that.pag=0;
      that.num=num;
      updateNums();

      $('.text-frame').off('click').on('click',function(ev){
        that.pag++;
        if(that.pag>that.num-1) that.pag=0;
        updateNums();
        ev.preventDefault();
        ev.stopPropagation();
      });

      function updateNums(){
        $('.paginacio li').removeClass("selected");
        $('.paginacio li[data-pag='+that.pag+']').addClass("selected");

        $('.text-frame .content').html(content[that.pag]);
      }

      $('.paginacio li').off('click').on('click',function(ev){

        that.pag=$(this).data('pag');

        updateNums();
        ev.preventDefault();
        ev.stopPropagation();

      });

    }else{
      $('.text-frame ').off('click');

      that.num=0;
      that.pag=0;
      $('.text-frame .paginacio').html('');
    }




    $('.text-frame .titol').css({top:'90px'});

    if(content.constructor != Array){
      $('.text-frame .content').html(content);
    }
    var titol=this.getTitle();

    if(titol==undefined){
      $('.text-frame .titol').hide();
    }else{
      $('.text-frame .titol').show();
    }


    var left=$('.text-frame .titol').css('left');


    $('.text-frame .titol').text(titol);
    $('.text-frame .titol').velocity(
      { "margin-left": [0,'1200px']

      },
      {  duration: 500,
        complete:function(){
          $('.text-frame .content').velocity(
            { opacity: [1,0] },
            {  duration: 500 }
          )}

      }
    );


  }
}



function Cortina(){
  this.tancada=true;
}

Cortina.prototype.obre=function(cb){

  var that=this;
  if(this.tancada){
    cb();
    this.do(-1);
    return;
  }

  this.do(1,function(){
    cb();
    setTimeout(function(){
    that.do(-1);
  },500);

  });

}

Cortina.prototype.do=function(dir,cb){

  switch(dir){
    case -1:
        $('.fase4 .cortina-superior').velocity(
          { top: ['-480px','0px'] },
          {  duration: 1000,
          complete:cb}
        );
          this.tancada=false;
            //playAudio("teatre/cortina_1");
        break;
    case 1:
        $('.fase4 .cortina-superior').velocity(
          { top: ['0px','-480px'] },
          {  duration: 1000,
          complete:cb}
        );
          this.tancada=true;
          //  playAudio("teatre/cortina_2");
        break;

    case 2:

        $('.fase4 .cortina-left').velocity(
          { left: '-60px' },
          {  duration: 1000,  complete:cb}
        );
        $('.fase4 .cortina-right').velocity(
          { left: '400px'},
          {  duration: 1000}
        );
        //  playAudio("teatre/cortina_1");
        break;
        case 3:

            $('.fase4 .cortina-left').velocity(
              { left: '-390px' },
              {  duration: 1000,  complete:cb}
            );
            $('.fase4 .cortina-right').velocity(
              { left: '800px'},
              {  duration: 1000}
            );
            //  playAudio("teatre/cortina_2");
            break;

  }

}





function Preload(){

}

Preload.prototype.init=function(images){
  var that=this;
  this.images=images;
  this.count=images.length;
  this.loaded=0;
  for(var i=0;i<images.length;i++){
      var im=new Image();
      im.src="images/"+images[i];
      im.onload=function(){
        that.loaded++;


        var pc=Math.floor(that.loaded*100/that.count);

        $('.fase0 .loading .barra').css('width',pc+'%');

        $('.fase0 .barra .msg').text(pc+"%");
        if(that.loaded==that.count){
          $('.fase0 .loading').fadeOut(2000);
        }
      };
  }
}

var pre=new Preload();

pre.init([
  "bg-rombos.png",
"bk_biografia.jpg",
"bk_cinematograf.jpg",
"bk_estudi.jpg",
"bk_globus.jpg",
"bk_text.png",
"boto.png",
"filet.png",
"filigrana_dreta.png",
"filigrana_esquerra.png",
"ico_left.png",
"ico_right.png",
"llumeta.png",
"marc_text.png",
"estudi/estudi_cofre_obert.png"]
);
