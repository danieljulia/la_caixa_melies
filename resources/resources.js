
var textos=[
  {"name":"globus",
  data:[
    {title_ca:"El París del segle XIX",
    title_es:"El París del siglo XIX"},

    {title_ca:"Ciència i progrés",
    title_es:"Ciencia y progreso",
    ico:"paris/paris_ico_1.png",
    image:"paris/paris_diapo_1.png",
    caption_ca:"El gran globus de Mr. Henri Giffard en París, 1878.",
    caption_es:"El gran globo de Mr. Henri Giffard en París, 1878.",
    text_ca:"<p>Méliès va viure en una època d’esplendor científica i de confiança plena en el progrés, un món canviant que evolucionava a passos gegantins gràcies als nous descobriments científics i de la seva aplicació en la indústria.</p> <p> Al llarg del segle xix, es van inventar la locomotora, la làmpada incandescent, el telèfon, els primers dirigibles, l’aviació... Es van descobrir els microorganismes i l’aspirina. L’any 1859, es va publicar <i>L’origen de les espècies</i>, de Charles Darwin, una obra cabdal de la biologia que va revolucionar la ciència amb la teoria evolutiva de les espècies.</p> <p> La imatge del científic boig o la dels zepelins van inspirar més d’una de les seves pel·lícules.</p>",
    text_es:"<p>Méliès vivió en una época de esplendor científico y de confianza plena en el progreso, un mundo cambiante que evolucionaba a pasos agigantados gracias a los nuevos descubrimientos científicos y  su aplicación en la industria.</p><p>A lo largo del siglo XIX se inventó la locomotora, la lámpara incandescente, el teléfono, los primeros dirigibles, la aviación... Se descubrieron los microorganismos y la aspirina. En el año 1859 se publicó <i>El origen de las especies</i>, de Charles Darwin, una obra capital de la biología que revolucionó la ciencia con la teoría evolutiva de las especies.</p><p>La imagen del científico loco o la de los zepelines inspiraron más de una de sus películas.</p>"
  },
  {title_ca:"Un món per descobrir",
  title_es:"Un mundo por descubrir",
  ico:"paris/paris_ico_2.png",
  image:"paris/paris_diapo_2.png",
  caption_ca:"Membres de l'expedició Terra Nova al Pol Sud, 1912.",
  caption_es:"Miembros de la expedición Terra Nova en el Polo Sur, 1912.",
  text_ca:"<p>Al segle XIX, als països occidentals hi havia un esperit d’aventura i fascinació per explorar indrets desconeguts i exòtics. Al planeta, encara hi havia molts paratges i moltes cultures per descobrir, i el repte consistia a posar a prova els límits de l’ésser humà, com ho van demostrar les expedicions antàrtiques o les aventures de Livingstone a l’Àfrica.</p><p>El món es tornava cada cop més petit, i la gent de l’època tenia curiositat per conèixer aquests mons llunyans i exòtics. El cinema era el mitjà que permetia als espectadors fer aquests viatges sense moure’s de la cadira.</p>",
  text_es:"<p>En el siglo XIX, en los países occidentales, se vivía con un espíritu de aventura y fascinación por explorar lugares desconocidos y exóticos. En el planeta todavía existían muchos parajes y muchas culturas por descubrir, y el reto consistía en poner a prueba los límites del ser humano, como demostraron las expediciones antárticas o las aventuras de Livingstone en África.</p><p>El mundo se volvía cada vez más pequeño, y la gente de la época tenía curiosidad por conocer todos esos mundos lejanos y exóticos. El cine era el medio que permitía a los espectadores realizar esos viajes sin moverse de la silla.</p>"
},  {title_ca:"Literatura fantàstica",
  title_es:"Literatura fantástica",
  ico:"paris/paris_ico_3.png",
  image:"paris/paris_diapo_3.png",
  caption_ca:"<i>Els primers homes a la luna</i> de H.G. Wells,1902. ",
  caption_es:"<i>Los primeros hombres en la luna</i> de H.G. Wells,1902. ",
  text_ca:"<p>Els avenços científics i tècnics de mitjan segle XIX van propiciar la consolidació d’un nou gènere literari: la literatura fantàstica o de ciència-ficció. </p><p>Méliès n’era un gran lector i, fins i tot, va traslladar a la gran pantalla algunes aventures de les novel•les d’autors fonamentals d’aquest gènere, com Edgar Allan Poe, Jules Verne o H.G. Wells.</p>",
  text_es:"<p>Los progresos científicos y técnicos de mediados de siglo XIX propiciaron la consolidación de un nuevo género literario: la literatura fantástica o de ciencia ficción.</p><p>Méliès era un gran lector e incluso trasladó a la gran pantalla algunas aventuras de las novelas de autores fundamentales de este género, como Edgar Allan Poe, Julio Verne o H.G. Wells. </p>"
},
{title_ca:"Oci i entreteniment",
title_es:"Ocio y entretenimiento",
ico:"paris/paris_ico_4.png",
image:"paris/paris_diapo_4.png",
caption_es:"Teatro Óptico de Reynaud en París, 1892.",
caption_ca:"Teatre Òptic de Reynaud a París, 1892.",
text_ca:["<p>A finals del segle XIX, París era una ciutat industrialitzada, moderna i, tot i que les jornades laborals eren molt llargues, van començar aparèixer activitats per gaudir del temps d’oci familiar.</p><p>Avui dia passem moltes hores de lleure davant de pantalles que ens ofereixen imatges en moviment –el cinema, Internet o els videojocs–, però aleshores no existien aquests invents. Com es divertia la gent del París del segle XIX? </p><p>Hi havia diversos espectacles, des del teatre tradicional a fins a les <i>féerie</i> (gènere de teatre fantàstic, un bon exemple del qual és l’obra <i>Voyage dans la Lune</i> representada el 1877) o els teatres de varietats. </p>"," <p> L’èxit i la repercussió de públic se l’enduien les fires ambulants que oferien atraccions de tota mena, moltes d’elles relacionades amb les il·lusions òptiques. La gent anhelava veure imatges en moviment i ho podien fer gràcies a joguines i dispositius com, per exemple, la llanterna màgica. Amb ella es feien espectacles de fantasmagories, en els quals el públic entrava a una sala on començaven a passar fenòmens estranys, sorolls i projeccions terrorífiques amb moviment (que es feien gràcies als seu sistema de vidres pintats i superposats).</p> "],
text_es:["<p>A finales del siglo XIX, París era una ciudad industrializada, moderna y, a pesar de que las jornadas laborales eran muy largas, empezaron a aparecer actividades para disfrutar del tiempo de ocio familiar. Hoy en día pasamos muchas horas de ocio ante pantallas que nos ofrecen imágenes en movimiento –el cine, internet o los videojuegos–, pero entonces no existían esos inventos. ¿Cómo se divertía la gente del París del siglo XIX?</p><p>Había varios espectáculos, desde el teatro tradicional hasta las <i>féerie</i> (género de teatro fantástico, del cual un buen ejemplo sería la obra <i>Voyage dans la Lune</i> representada en 1877) o el teatro de variedades.</p>","<p>El éxito y la repercusión de público se lo llevaban las ferias ambulantes, que ofrecían atracciones de todo tipo, muchas relacionadas con las ilusiones ópticas. La gente anhelaba ver imágenes en movimiento y lo lograban con juguetes y dispositivos, como la linterna mágica. Con ella se realizaba espectáculos de fantasmagorías, en los que el público entraba en una sala donde empezaban a pasar fenómenos extraños, ruidos y proyecciones terroríficas con movimiento (gracias a su sistema de cristales pintados y superpuestos).</p>"]
}
]},

{"name":"biografia",
data:[

  {title_ca:"Biografia",
  title_es:"Biografía"},

  {title_ca:"Els primers anys",
  title_es:"Los primeros años",
  ico:"bio/bio_ico_1.png",
  image:"bio/bio_diapo_1.png",
  caption_es:"Fábrica de zapatos de Jean-Louis Méliès, c.1881.",
  caption_ca:"Fàbrica de sabates de Jean-Louis Méliès, c.1881.",
  text_ca:["<p>Georges Méliès va néixer a París el 8 de desembre de 1861, una època en què encara no existia el cinema!</p><p>El pare, fabricant de calçat de luxe, esperava que el seu fill continués amb el negoci familiar, però des de ben petit a Georges Méliès el que li agradava eren les activitats creatives com dibuixar.</p><p>El pare el va enviar a estudiar a Londres , per fer-li oblidar les seves inquietuds artístiques, però allà va descobrir el món de l’il·lusionisme i la màgia gràcies als espectacles del mag Maskelyne a l’Egyptian Hall.</p> ","<p>En tornar a París, estava decidit a estudiar Belles Arts, però la família el va obligar a treballar a l’empresa familiar. Quan el pare es va jubilar del negoci, ell es va negar a continuar, i amb els diners que va rebre de la família va decidir comprar el Teatre Robert-Houdin per fer realitat el seu somni: ser mag.</p>"],
  text_es:["<p>Georges Méliès nació en París el 8 de diciembre de 1861, una época en la que... ¡aún no existía el cine!</p><p>Su padre, fabricante de calzado de lujo, esperaba que su hijo continuara con el negocio familiar. No obstante, desde muy pequeño, lo que le gustaba a Georges eran las actividades creativas, como dibujar.</p><p>Su padre lo mandó a estudiar a Londres para hacerle olvidar sus inquietudes artísticas, pero allí descubrió el mundo del ilusionismo y la magia gracias a los espectáculos del mago Maskelyne en el Egyptian Hall.</p>","<p>Al regresar a París, estaba decidido a estudiar Bellas Artes, pero su familia le obligó a trabajar en la empresa familiar. Cuando su padre se jubiló del negocio, él se negó a continuar, y con el dinero que recibió de la familia decidió comprar el teatro Robert-Houdin para hacer realidad su sueño: ser mago.</p>"]
  },

  {title_ca:"Méliès i la màgia",
  title_es:"Méliès y la magia",
  ico:"bio/bio_ico_2.png",
  image:"bio/bio_diapo_2.png",
  caption_es:"Méliès con una máscara de Beelzebub, c. 1890. ",
  caption_ca:"Méliès amb una màscara de Beelzebub, c. 1890. ",
  text_ca:"<p>Amb l’adquisició del Teatre Robert-Houdin, el juliol de 1888, Georges Méliès es va convertir en un mag reconegut al París de finals de segle.</p><p> Allà ja va demostrar la seva capacitat imaginativa amb la creació dels seus espectacles farcits de sorpreses. També la seva destresa en la construcció, ja que fabricava la majoria dels decorats, trucs i maquinàries que apareixien dalt de l’escenari.</p><p>Alguns dels seus millors trucs van ser el sainet de màgia “L’armari del “El decapitat recalcitrant” i el truc “El nan groc”, en el qual tancaven un nen disfressat dins d’una maleta, la posaven a un armari i, quan l’obrien, el nen havia aconseguit sortir.</p>",
  text_es:"<p>Con la adquisición del teatro Robert-Houdin en julio de 1888, Georges Méliès se convirtió en un mago reconocido en el París de fin de siglo.</p><p>Allí demostró su capacidad imaginativa con la creación de unos espectáculos repletos de sorpresas. También su destreza en la construcción, ya que él mismo fabricaba la mayoría de decorados, trucos y máquinas que aparecían sobre el escenario.</p><p>Algunos de sus mejores trucos fueron el sainete de magia \"El decapitado recalcitrante\" y \"El enano amarillo\", en el que encerraban a un niño disfrazado dentro de una maleta, la colocaban dentro de un armario y, cuando lo abrían, el enano niño había logrado salir.</p>"
  },

  {title_ca:"La descoberta del cinema",
  title_es:"El descubrimiento del cine",
  ico:"bio/bio_ico_3.png",
  image:"bio/bio_diapo_3.png",
  caption_es:"Cartel <i>Cinématographe Lumière</i>, 1895. ",
  caption_ca:"Cartell <i>Cinématographe Lumière</i>, 1895. ",
  text_ca:["<p> Georges Méliès seguia de prop les innovacions tecnològiques relacionades amb l’il·lusionisme i els espectacles per incorporar-les al seu teatre. Per exemple, feia projeccions de llanterna màgica en què els espectadors podien veure petites animacions gràcies a la projecció de plaques pintades i superposades.</p>","<p> Fins que el 1895 va succeir un fet que va canviar la seva vida: Méliès va assistir a la primera projecció del cinematògraf inventat pels germans Lumière. Es va quedar tan fascinat amb <i>La Sortie des usines Lumière</i> (La sortida de la fàbrica Lumière) i <i>L’Arrivée d'un train à La Ciotat</i> (L’arribada d’un tren a La Ciotat) que, des d’aquell primer moment, va tenir clar que havia d’aconseguir un cinematògraf.</p><p> Però els germans Lumière no l’hi van vendre. “Aquest invent no té futur”, li van dir. I Méliès va haver de viatjar a Londres per comprar una càmera a Robert William Paul.</p>"],
  text_es:["<p>Georges Méliès seguía de cerca las innovaciones tecnológicas relacionadas con el ilusionismo y los espectáculos para incorporarlas a su teatro. Por ejemplo, hacía proyecciones con linterna mágica en las que los espectadores podían ver pequeñas animaciones gracias a la proyección de placas pintadas y superpuestas.</p>","<p>Hasta que en 1895 sucedió un hecho que cambió su vida: Méliès asistió a la primera proyección del cinematógrafo inventado por los hermanos Lumière. Quedó tan fascinado con <i>La Sortie des usines Lumière</i> (La salida de la fábrica Lumière) y <i>L'Arrivée d'un train à la Ciotat</i> (La llegada de un tren a La Ciotat) que, desde aquel primer instante, tuvo claro que tenía que conseguir un cinematógrafo.</p><p>Pero los hermanos Lumière no se lo vendieron. \"Este invento no tiene futuro\", le dijeron. Y Méliès tuvo que viajar a Londres para comprar una cámara a Robert William Paul.</p>"]
  },

  {title_ca:"Méliès cineasta",
  title_es:"Méliès cineasta",
  ico:"bio/bio_ico_4.png",
  image:"bio/bio_diapo_4.png",
  caption_es:"<i>El Viaje a la Luna</i>. Recomposición de escena, c. 1930.",
  caption_ca:"<i>El Viatge a la Lluna</i>. Recomposició d'escena, c. 1930.",
  text_ca:["<p>El 1896 Méliès va projectar les seves primeres pel·lícules al Teatre Robert-Houdin. Eren retrats de la realitat a l’estil dels germans Lumière, tot i que no va trigar a rodar la primera pel·lícula de trucatges: <i>Escamotage d’une dame chez Robert-Houdin</i> (Escamoteig d’una dama al Robert-Houdin), on va combinar la màgia de l’il·lusionisme amb el cinema. S’obria un nou món de possibilitats!</p><p> Així van començar per a Méliès va iniciar uns anys frenètics en els quals va filmar més de 500 pel·lícules, va crear la seva productora, la Star Film, va inaugurar el primer estudi de cinema i va inventar els trucatges cinematogràfics.</p> ","<p> Méliès es va convertir en l’home orquestra del cinema: ell mateix construïa la seva càmera, feia els guions, filmava les pel·lícules, actuava interpretant diversos personatges, pintava els decorats, dirigia els actors, s’inventava els trucatges i els efectes especials, feia el muntatge final i venia les pel·lícules a metres als firaires.</p> "],
  text_es:["<p>En 1896 Méliès proyectó sus primeras películas en el Teatro Robert-Houdin. Eran retratos de la realidad al estilo de los hermanos Lumière, a pesar de que no tardó en rodar la primera película de trucajes: <i>Escamotage d'une dame chez Robert-Houdin</i> (Escamoteo de una dama en el Robert-Houdin), donde combinó la magia del ilusionismo con el cine. ¡Se abría un nuevo mundo de posibilidades!</p><p>Así comenzaron para Méliès unos años frenéticos en los que filmó más de 500 películas, creó su productora la Star Film, inauguró el primer estudio de cine e inventó los trucajes cinematográficos.</p>","<p>Méliès se convirtió en el hombre orquesta del cine: él mismo construía su cámara, filmaba las películas, escribía los guiones, actuaba interpretando varios personajes, pintaba los decorados, realizaba el montaje final y vendía las películas a metros a los feriantes.</p>"]
  },

  {title_ca:"La desaparació de la Star Film",
  title_es:"La desaparición de la Star Film",
  ico:"bio/bio_ico_5.png",
  image:"bio/bio_diapo_5.png",
  caption_es:"Logotipo de Star Film.<br>    ",
  caption_ca:"Logotip d'Star Film.<br>    ",
  text_ca:["<p> Méliès va ser un dels pioners del cinema: va inaugurar un art que es va convertir en indústria, però no va ser capaç d’adaptar-se a l’evolució del negoci, als nous gustos dels espectadors i al nou funcionament de la indústria de la principal activitat d’oci del segle XX: el cinema.</p>","<p> Els deutes i la dificultat de competir amb les grans productores, com Pathé i Gaumont, el van portar a malvendre tot el negoci, l’estudi de Montreuil i el Teatre Robert-Houdin. A més a més, en un atac de ràbia va destruir els negatius de les seves pel·lícules. Semblava que tots els anys d’esforç i treball dedicats a l’art cinematogràfic desapareixien sense deixar rastre, i les pel·lícules de Méliès passaven a ser un record del passat, dels inicis d’un art nou que havia nascut per enlluernar el públic i divertir-lo.</p>","<p> Per sort, s’ha pogut recuperar la major part de les seves pel·lícules. Altrament, hauria estat una gran pèrdua per a la història del cinema!</p>"],
  text_es:["<p>Méliès fue uno de los pioneros del cine: inauguró un arte que se convirtió en industria, pero no fue capaz de adaptarse a la evolución del negocio, a los nuevos gustos de los espectadores y al nuevo funcionamiento de la principal actividad de ocio del siglo XX: el cine.</p><p>Las deudas y la dificultad para competir con las grandes productoras, como Pathé y Gaumont, lo llevaron a malvender todo el negocio, el estudio de Montreuil y el teatro Robert-Houdin, además, en un ataque de rabia destruyó los negativos de sus películas.</p>","<p>Parecía que todos los años de esfuerzo y trabajo dedicados al arte cinematográfico desaparecían sin dejar rastro y las películas de Méliès pasaban a ser un recuerdo del pasado, de los inicios de un arte nuevo que había nacido para deslumbrar al público y divertirle.</p><p>Afortunadamente, se ha podido recuperar la mayor parte de sus películas. De lo contrario ¡Habría sido una gran pérdida para la historia del cine!</p>"]
  },

  {title_ca:"La botiga de Montparnasse i el redescobriment",
  title_es:"La tienda de Montparnasse y el redescubrimiento",
  ico:"bio/bio_ico_6.png",
  image:"bio/bio_diapo_6.png",
  caption_es:"Méliès en la tienda de la estación de Montparnasse, c.1930.",
  caption_ca:"Méliès a la botiga de l'estació de Montparnasse, c.1930.",
  text_ca:["<p> A partir de 1925, Georges Méliès va regentar una petita botiga de joguines a l’estació de Montparnasse, una feina que, per a ell, una persona creativa i activa, era com estar en una presó.</p><p> I quan tothom es pensava que Méliès havia mort, Léon Druhot, un periodista que passava per l’estació, va escoltar com algú cridava el nom de Méliès. Quina sorpresa quan li va preguntar si tenia alguna relació amb el cineasta Georges Méliès: “Oh i tant que sí! I encara diria més: sóc el seu familiar més proper perquè, de fet, sóc jo Georges Méliès!”.</p>", "<p> Llavors es va organitzar una gran gala en honor seu, i la mútua del cinema li va oferir un habitatge a Orly perquè pogués viure la seva vellesa dignament. </p><p> Georges Méliès va morir a París el 21 de gener de 1938.</p>"],
  text_es:["<p>A partir de 1925, Georges Méliès regentó una pequeña tienda de juguetes en la estación de Montparnasse, un trabajo que, para él, una persona creativa y activa, era como estar encarcelado.</p><p>Y cuando todo el mundo creía que Méliès había muerto, Léon Druhot, un periodista que pasaba por la estación, escuchó a alguien gritar el nombre de Méliès. Cuál fue su sorpresa cuando le preguntó si tenía alguna relación con el cineasta Georges Méliès y este le respondió: “¡Oh, claro que sí! Y aún le diría más: soy su familiar más cercano porque, de hecho, ¡yo soy Georges Méliès!“.</p>","<p>Se organizó entonces una gran gala en su honor, y la mutualidad del cine le ofreció una residencia en Orly para que pudiera vivir su vejez dignamente.</p><p>Georges Méliès murió en París el 21 de enero de 1938.</p>"]
  },

]
},

{"name":"germans",
data:[
  {title_ca:"Els germans Lumière",
  title_es:"Los hermanos Lumière",
  text_ca:["<p>Auguste i Louise Lumière van ser els dos científics que van inventar el cinematògraf, un artefacte que permetia filmar imatges de la realitat i després projectar-les. Va ser el 1895 quan van presentar les primeres pel·lícules, <i>La Sortie des usines Lumière</i> (La sortida de la fàbrica Lumière) i (L’arribada d’un tren a La Ciotat).</p>","<p> Allà estava Méliès i es va quedar tan meravellat que els va voler comprar un cinematògraf. Però els germans Lumière no van accedir perquè consideraven que aquell invent no tindria futur i, a part, per a ells, tan sols tenia interès científic i no servia pel món de l’espectacle. </p>"],
  text_es:["<p>Auguste y Louis Lumière fueron los dos científicos que inventaron el cinematógrafo, un artefacto que permitía filmar imágenes de la realidad y después proyectarlas. Fue en 1895 cuando presentaron las primeras películas, <i>La Sortie des usines Lumière</i> (La salida de la fábrica Lumière) y <i>L'Arrivée d'un train à la Ciotat</i> (La llegada de un tren a La Ciotat).</p>","<p>Allí estaba Méliès, y se quedó tan fascinado que les quiso comprar un cinematógrafo. Pero los hermanos Lumière no accedieron porque consideraban que aquel invento no tenía futuro y, además, para ellos sólo tenía un interés científico y no servía para el mundo del espectáculo.</p>"]
}]},


{"name":"projector",
data:[
  {title_es:"Proyector",
  title_ca:"Projector",
  text_ca:["<p> Un cop el film estava muntat, ja es podia exhibir. El mateix cinematògraf també servia com a projector. Una làmpada molt potent projectava la imatge dels fotogrames a través de l’òptica, que augmentava i enfocava la imatge a la pantalla. I el so? </p>","<p> Entre 1895 i 1927 no s’havia desenvolupat encara la tecnologia que permetia enregistrar i reproduir el so; per tant, les pel·lícules eren mudes. Això no significa que a les projeccions de cinema no hi hagués so! A la sala, mai no faltava l’acompanyament d’un piano, la figura d’un narrador o, fins i tot, una orquestra que tocava música en directe.</p>"],
  text_es:["<p>Una vez montado el film ya se podía exhibir. El mismo cinematógrafo también servía como proyector. Una lámpara muy potente proyectaba la imagen de los fotogramas a través de la óptica, que aumentaba y enfocaba la imagen en la pantalla. ¿Y el sonido?</p>","<p>Entre 1895 y 1927 no se había desarrollado aún la tecnología que permitía grabar y reproducir el sonido, así que las películas eran mudas. ¡Pero eso no significa que en las proyecciones de cine no hubiera sonido! En la sala, nunca faltaba el acompañamiento de un piano, la figura de un narrador o, incluso, una orquesta que tocaba música en directo.</p>"]
},
  {title_es:"Obturación",
  title_ca:"Obturació",
  text_ca:"<p> Una làmina que, en el rodatge, regulava l’entrada de llum per enregistrar cadascun dels fotogrames i, en la projecció, tapava els canvis de fotograma.</p>",
  text_es:"<p>Una lámina que, durante el rodaje, regulaba la entrada de luz para registrar cada uno de los fotogramas y, en la proyección, tapaba los cambios de fotograma.</p>"
},
  {title_es:"Película",
  title_ca:"Pel·lícula",
  text_ca:"<p> Les primeres bobines de cel·luloide mesuraven al voltant dels 17 metres de longitud, la qual cosa permetia fer tan sols un minut de pel·lícula! Georges Méliès va introduir el color a les seves pel·lícules per guanyar espectacularitat. Això ho va aconseguir pintant a mà cada un dels fotogrames!</p>",
  text_es:"<p>Las primeras bobinas de celuloide medían alrededor de 17 metros de longitud, ¡lo cual permitía producir tan solo un minuto de película! Georges Méliès introdujo el color en sus películas para ganar en  espectacularidad. ¡Lo consiguió pintando a mano cada uno de los fotogramas! </p>"
},
  {title_es:"Manivela",
  title_ca:"Manovella",
  text_ca:"<p> Movent la manovella, es posaven en funcionament les rodes dentades que movien la cinta perforada de la pel·lícula. Es projectaven 16 fotogrames per segon.<p>",
  text_es:"<p>Moviendo la manivela, se ponían en funcionamiento las ruedas dentadas que movían la cinta perforada de la película. Se proyectaban 16 fotogramas por segundo.<p>"
}]},

{"name":"pelis",


data:[
  {title_ca:"Pel·lícules",
  title_es:"Películas",
  },
  {title:"Escamotage d'une dame",
  text_ca:"<p>El 1896, poc després de la primera projecció pública de cinema dels germans Lumière, George Méliès rodarà els seus primers films. En destaquem <i>Escamotage d’une dame chez Robert-Houdin</i> (Escamoteig d’una dama al Robert-Houdin), perquè és el primer film amb un trucatge, el truc de substitució, i que inaugura el cinema fantàstic. Abracadabra! Finalment, es feia realitat el somni de qualsevol il·lusionista! Un toc de vareta, unes paraules màgiques... i l’actriu que hi havia a l’escenari es transformava en un objecte! Amb aquest nou invent, el cinematògraf, s’obria un món de possibilitats creatives per explorar.</p><p>En aquest film, podem reconèixer l’humor macabre típic de Méliès, que hi actua com a mestre de cerimònies i converteix l’actriu esplèndida en un esquelet ben tètric. </p>",
  text_es:"<p>En 1896, poco después de la primera proyección pública de cine de los hermanos Lumière, George Méliès rueda sus primeros films. Destacamos <i>Escamotage d'une dame chez Robert-Houdin</i> (Escamoteo de una dama en el Robert-Houdin), porque es el primer film con un trucaje, el truco de sustitución que inaugura el cine fantástico. ¡Abracadabra! ¡Finalmente se hacía realidad el sueño de cualquier ilusionista! Un toque de varita, unas palabras mágicas... y la actriz que había en el escenario... ¡se transformaba en un objeto! Con este nuevo invento, el cinematógrafo, se abría un mundo de posibilidades creativas por explorar.</p><p>En este film, podemos reconocer el humor macabro típico de Méliès, que actúa como maestro de ceremonias y convierte la actriz espléndida en un esqueleto bien tétrico.</p>"
},
  {title:"Le Mélomane",
  text_ca:"<p>Un melòman és una persona apassionada per la música. Quin és l’homenatge a la música que imagina George Méliès? És un número de màgia ben esbojarrat: un pentagrama d’on quedaran encordats els caps decapitats del mateix Méliès, que són les notes i les corxeres d’aquesta peculiar partitura musical.</p><p><i>Le Mélomane</i> (El melòman) és un film atrevit, estrafolari i també és un dels curts més prodigiosos i complexos de la filmografia de Méliès per la meticulosa combinació dels trucatges de substitució i de sobreimpressió. Requeria una bona planificació de rodatge!</p>",
  text_es:"<p>Un melómano es una persona apasionada por la música. ¿Cuál es el homenaje a la música que imagina George Méliès? Es un número de magia muy alocado: un pentagrama donde quedarán atadas las cabezas decapitadas del propio Méliès, que son las notas y las corcheas de esta peculiar partitura musical.</p><p><i>Le Mélomane</i> (El melómano) es un film atrevido, estrafalario y también es uno de los cortos más prodigiosos y complejos de la filmografía de Méliès por la meticulosa combinación de los trucajes de sustitución y de sobreimpresión. ¡Requería una planificación de rodaje muy precisa!</p>"
},
  {title:"L'Homme Orchestre",

  text_ca:"<p>Aquest film és un altre homenatge a la música i demostra el talent de Méliès en el truc de la sobreimpressió. Set cadires a escena i set instrumentistes en un mateix pla, interpretats pel mateix Méliès;a banda d’interactuar entre ells,els músics apareixen i desapareixen per art de màgia. No oblidem que aquesta obra es va projectar l’any 1900! Sens dubte, Méliès va ser pioner a l’hora de concebre el cinema com a espectacle, com a entreteniment, una concepció que s’ha mantingut fins a dia d’avui.</p>",
  text_es:"<p>Este film es otro homenaje a la música y demuestra el talento de Méliès con el truco de la sobreimpresión. Siete sillas en escena y siete instrumentistas en un mismo plano, interpretados por el propio Méliès; además de interactuar entre ellos, los músicos aparecen y desaparecen por arte de magia. ¡No olvidemos que esta obra se proyectó en 1900! Sin duda, Méliès fue pionero en la concepción del cine como espectáculo, como entretenimiento, una concepción que se ha mantenido hasta día de hoy.</p>"
},
{title:"Le voyage de Gulliver",

text_ca:"<p>A <i>Els viatges de Gulliver</i>, novel·la de Jonathan Swift publicada el 1726, hi ha indrets exòtics i personatges fascinants com els habitants de Lil·liput, que són dotze vegades més petits que els humans. Un mag com Méliès no podia deixar escapar l’oportunitat de plasmar en imatges les aventures de Gulliver amb els lil·liputencs! Per aconseguir aquest engany visual, Méliès se les va empescar amb el trucatge de sobreimpressió que requeria dividir la pantalla i rodar les escenes dels lil·liputencs amb molta distància de càmera. D’aquesta manera aconseguia canviar l’escala real dels personatges.</p>",
text_es:"<p><li>En Los viajes de Gulliver</i>, novela de Jonathan Swift publicada en 1726, aparecen lugares exóticos y personajes fascinantes como los habitantes de Liliput, que son doce veces más pequeños que los humanos. ¡Un mago como Méliès no podía dejar escapar la oportunidad de plasmar en imágenes las aventuras de Gulliver con los liliputienses! Para lograr este engaño visual, Méliès se las ingenió con el trucaje de la sobreimpresión que requería dividir la pantalla y rodar las escenas de los liliputienses a mucha distancia de cámara. De este modo lograba cambiar la escala real de los personajes.</p>"
},
{title:"Les 400 farces du Diable",

text_ca:"<p>El diable es disfressa d’alquimista i ofereix dues píndoles màgiques a un enginyer i el seu ajudant. Els promet que podran fer un viatge arreu del món, però el dimoni vol les seves ànimes, i els viatgers començaran una cursa astral molt trepidant dins d’una carrossa alada conduïda per un cavall mecànic. Finalment, una caiguda vertiginosa els conduirà cap a l’infern.</p><p>En aquest film, la proposta estètica de Méliès recorda les fantasmagories d’èpoques anteriors, amb transparències, titelles i un teló de fons sense fi que dóna moviment a l’acció. És un film encara molt teatral, però les troballes visuals, entre oníriques i macabres, són d’una bellesa que ens captiva.</p>",
text_es:"<p>El diablo se disfraza de alquimista y ofrece dos píldoras mágicas a un ingeniero y su ayudante. Les promete que podrán hacer un viaje alrededor del mundo, pero el demonio quiere sus almas, y los viajeros empezarán una trepidante huída astral dentro de una carroza alada conducida por un caballo mecánico. Finalmente, una caída vertiginosa los conducirá hacia el infierno.</p><p>En este film, la propuesta estética de Méliès recuerda las fantasmagorías de épocas anteriores, con transparencias, títeres y un telón de fondo sin fin que da movimiento a la acción. Es una película aún muy teatral, pero los aciertos visuales, entre oníricos y macabros, son de una belleza que nos cautiva.</p>"
},
  {
  title:"Voyage dans la lune",
  text_ca:"<p><i>Voyage dans la Lune</i> (Viatge a la Lluna) és un dels primers films de ficció de la història del cinema i té l'honor de ser reconeguda com la primera creació cinematogràfica de ciència-ficció. És l'obra mestra de Méliès perquè recull moltes de les seves aportacions al setè art, tant en els aspectes artístics, d'una gran sensibilitat visual, com en els tècnics dels trucatges.</p><p>La història es basa en dues novel·les, <i>De la terra a la lluna</i>, de Jules Verne, i <i>Els primers homes a la lluna</i>, de H.G. Wells, i explica les aventures d'un grup de científics que organitzen una expedició a la Lluna.</p><p>Potser no es recorden tant els bolets gegants, les divinitats celestials o els selenites que volen capturar els científics, però la imatge del coet que aterra i empastifa l'ull dret de la lluna s'ha convertit en una de les icones més representatius de la història del cinema.</p>",
  text_es:"<p><i>Voyage dans la Lune</i> (Viaje a la Luna) es uno de los primeros films de ficción de la historia del cine y tiene el honor de ser reconocida como la primera creación cinematográfica de ciencia ficción. Es la obra maestra de Méliès porque recoge muchas de sus aportaciones al séptimo arte, tanto en los aspectos artísticos, de una gran sensibilidad visual, como en los técnicos de los trucajes.</p><p>La historia se basa en dos novelas, <i>De la tierra a la luna</i>, de Julio Verne, y <i>Los primeros hombres a la luna</i>, de H.G. Wells, y cuenta las aventuras de un grupo de científicos que organizan una expedición a la Luna.</p><p>Quizás no se recuerdan tanto las setas gigantes, las divinidades celestiales o los selenitas que quieren capturar a los científicos, pero la imagen del cohete que aterriza y embadurna el ojo derecho de la luna se ha convertido en uno de los iconos más representativos de la historia del cine.<p>"
}]},



{"name":"casa",
data:[
  {
  text_ca:"<p>Davant de la dificultat de rodar a l’exterior, Méliès va idear el primer estudi cinematogràfic que, a més, era tot de vidre per deixar entrar la llum del sol i poder filmar. Aquest va ser el seu regne de somnis on va filmar la majoria de les seves pel·lícules.</p>",
  text_es:"<p>Ante la dificultad de rodar en exteriores, Méliès ideó el primer estudio de cine que, además, era todo de cristal para dejar entrar la luz del sol y poder filmar. Este fue su reino de sueños donde rodó la mayoría de sus películas.</p>"
},
{
text_ca:"<h2>Estudi:</h2><p> Era un rectangle de 6 metres d’alçada, 13,5 de llargada i 6,60 d’amplada. Estructura de ferro i envidrat per deixar entrar la llum del sol.",
text_es:"<h2>Estudio:</h2><p>Era un rectángulo de 6 metros de altura, 13,5 de longitud y 6,60 de anchura. Estructura de hierro y acristalado para dejar entrar la luz del sol."
},
{
text_ca:"<h2>Escenari:</h2><p> Méliès va dissenyar l’escenari amb la mateixa dimensió que el Teatre Robert-Houdin. Podem apreciar la concepció teatral de totes les seves pel·lícules en què la posició de la càmera és la de l’espectador de teatre.",
text_es:"<h2>Escenario:</h2><p>Méliès diseñó el escenario con las mismas dimensiones que el del teatro Robert-Houdin. Podemos apreciar la concepción teatral de todas sus películas, en las que la posición de la cámara es la misma que la del espectador en el teatro."
},
{
text_ca:"<h2>Sostre envidrat:</h2><p> Per deixar entrar la llum, el sostre era de vidre i l’estudi estava orientat per assolir el màxim d’hores de llum i, per tant, de treball. Hi havia uns finestrons mòbils per regular la claror que hi entrava.",
text_es:"<h2>Techo acristalado:</h2><p>Para dejar entrar la luz, el techo era de cristal y el estudio estaba orientado para lograr el máximo de horas de luz y, por lo tanto, de trabajo. Había unos ventanales móviles para regular la luminosidad."
},
{
text_ca:"<h2>Decorats:</h2><p> Telons, <i>trompe l’oeil</i> i tots els elements necessaris per a les seves escenografies. Méliès va fer construir una fossa de 3 metres amb trapes per fer aparèixer personatges fantàstics, va fer construir dos annexos per als bastidors i va instal·lar una passarel·la per fer volar fades.",
text_es:"<h2>Decorados:</h2><p>Telones, trampantojos y todos los elementos necesarios para sus escenografías. Méliès hizo construir, primero, una foso de 3 metros con trampillas para hacer aparecer personajes fantásticos; posteriormente, añadió dos anexos para los bastidores e instaló una pasarela para hacer volar a las hadas."
},
{
text_ca:["<h2> Posició de la càmera:</h2><p> Va construir un annex que guanyava distància i es podia convertir en una cambra obscura per poder manipular les pel·lícules sense que es velessin durant el procés de revelat i de muntatge."," <p>Avui dia, moltes pel·lícules es roden i es projecten amb tecnologia digital, però la història del cinema s’ha gestat fonamentalment gràcies a la cinta de pel·lícula química. </p>","<p> Es tracta d’un material molt sensible que ha de passar per diversos processos abans de poder ser projectat. La manipulació era manual. En primer lloc, s’havia de revelar i després s’anaven tallant i enganxant els fotogrames per donar coherència a la narració o per aconseguir els trucatges desitjats. Això és el que coneixem com el procés de muntatge.</p> "],
text_es:["<h2>Posición de la cámara:</h2><p>Construyó un anexo con el que ganaba distancia y se podía convertir en una cámara oscura para poder manipular las películas sin que se velasen durante el proceso de revelado y de montaje. ","Hoy en día, muchas películas se ruedan y se proyectan con tecnología digital, pero la historia del cine se ha gestado fundamentalmente gracias a la cinta de película química.</p>","<p>Se trata de un material muy sensible que tiene que pasar por varios procesos antes de poder ser proyectado. La manipulación era manual. En primer lugar, se tenía que revelar y después se iban cortando y pegando los fotogramas para dar coherencia a la narración o para conseguir los trucajes deseados. Eso es lo que conocemos como proceso de montaje.</p>"]
},
{
text_ca:"<h2>Entrades laterals:</h2><p> Com que cada cop feia servir més actors, va construir unes entrades laterals per facilitar-los l’accés a l’escenari. Fins i tot, a vegades els feia sortir per un lateral, donar la volta a l’estudi i tornar a entrar per l’altre costat per desfilar diverses vegades davant de la càmera i produir així la sensació de multitud.</p>",
text_es:"<h2>Entradas laterales:</h2><p> Como utilizaba cada vez más actores, construyó unas entradas laterales para facilitarles el acceso al escenario. A veces incluso los mandaba salir por un lateral, rodear el estudio y volver a entrar por el otro lado para desfilar varias veces por delante de la cámara y generar así la sensación de multitud."
}]}

];



var labels={
  titol_es:"El universo de Georges Méliès",
  titol_ca:"L'univers de Georges Méliès",
  paris_es:"París en el siglo XIX",
  paris_ca:"París al segle XIX",
  magiacine_es:"De la magia al cine",
  credits_es:"Créditos",
  pelis_es:"Películas",
  pelis_ca:"Pel·lícules",
  peli_1:"L'escamotage d'une dame",
  peli_2:"Le mélomane",
  peli_3:"L'homme orchestre",
  peli_4:"Le voyage de Gulliver",
  peli_5:"Les 400 farces du Diable",
  peli_6:"Voyage dans la lune ",
  peli_1_code:"f7-x93QagJU",
  peli_2_code:"qGRH3VVrNFk", //uvfwgA6mBu0 no està disponible
  peli_3_code:"3RMp32GPWww", //TBn95rjP638 no està disponible
  peli_4_code:"dGg9j0BdyEM",//tUgpgZB7loE",
  peli_5_code:"5RsmNEi8a1o", //8udPRJkBvMc no està disponible
  peli_6_code:"_FrdVdKlxUk", //aNcxCR7f2MQ",
  siglo19_es:"Siglo XIX",
  siglo20_es:"Siglo XX",
    cinem_es:"El cinematógrafo",
    magiacine:"De la magia al cine",
    camara_es:"La cámara",
    germans_es:"Los hermanos Lumière",
    trucatges_es:"Trucajes",
    truc_subs_es:"Truco de</p><p> sustitución",
    truc_subs_info_es:"<p>Méliès fue pionero a usarlo. Cuenta la leyenda que fue fruto de una casualidad cuando, en un rodaje en la plaza de la Ópera, la manija de la cámara quedó trabada mientras filmaba un carruaje. Cuando reveló la película, se dio cuenta de que la imagen del carruaje desaparecía. </ P> <p> Este es el secreto del truco: detener la cámara durante la grabación e incorporar o quitar algún objeto o personaje de la escena. Una vez quedaba dentro o fuera de la escena, se continuaba grabando en el punto exacto y el resultado era espectacular. Parecía que los personajes desaparecían o aparecían de la nada!</p>",
    truc_esc_es:"Juegos de escala",
    truc_esc_info_es:"<p>Cuando miramos un paisaje de lejos, ¿verdad que parece que podríamos coger un pedazo con los dedos? En el cine, la distancia con los objetos permite hacer muchos trucos visuales. Son los juegos de escala y se pueden hacer de tres maneras:<ul><li>Filmando dos objetos o actores que están a distancias distintas y así se confunde al espectador sobre sus dimensiones.</li><li>Empleando maquetas, sobre todo de edificios, ciudades, monstruos... que parecerán de tamaño real, o bien decorados u objetos fuera de escala que harán que los actores nos parezcan personajes gigantes o diminutos.</li><li>En algunos casos Méliès utilizó la sobreimpresión: dos imágenes grabadas en momentos diferentes que después aparecen combinadas a la pantalla.</li></p>",

    truc_art_es:"Artefactos",
    truc_art_info_es:"<p>Méliès era un hombre de escena y estaba familiarizado con la tramoya teatral y los artefactos mecánicos que se utilizaban para hacer los cambios de decorados, las trampillas, el foso, los telones, los muebles trucados y los autómatas. Esa experiencia fue de lo más ventajosa en sus filmaciones, así como sus conocimientos técnicos sobre maquinaria, adquiridos en la empresa paterna. En el estudio de Montreuil, demostró que era un mago muy diestro: ideó artefactos, muñecos y engranajes mecánicos prodigiosos que supusieron un salto cualitativo para el mundo del cine.</p>",
    truc_sob_es:"Truco de </p><p>sobreimpresión",
    truc_sob_info_es:"<p>La sobreimpresión se usaba para que se vieran juntas en pantalla imágenes que se habían filmado por separado. Eso permitía, por ejemplo, desdoblar a un personaje. En el momento del rodaje, se podía crear este efecto rebobinando la cinta y grabando de nuevo en el mismo celuloide. Se podían crear efectos muy logrados si se rodaba sobre un fondo negro. En el momento del revelado, las dos imágenes quedaban sobreimpresas. El espectador veía como las imágenes grabadas en distintos momentos aparecían juntas a la pantalla.</p>",
    trucatgesinfo_ca:"Georges Méliès va ser el creador dels trucatges cinematogràfics, allò que avui en dia anomenem efectes especials. Va inaugurar el cinema fantàstic i de ciència-ficció. Cada pel·lícula era una oportunitat per posar a prova les seves habilitats i sorprendre el públic.",

    trucatgesinfo_es:"Georges Méliès fue el creador de los trucajes cinematográficos, lo que hoy en día denominamos <i> efectos especiales </i>. Inauguró el cine fantástico y de ciencia ficción. Cada película era una oportunidad para poner a prueba sus habilidades y sorprender al público.",
    trucatges_info_es:"Méliès era un hombre de escena y estaba familiarizado con la tramoya teatral y los remontes mecánicos que se utilizaban para hacer los cambios de decorados, las trampillas, la fosa, los telones, los muebles llamados y los autómatas. Esta experiencia fue de lo más ventajosa en sus filmaciones, así como sus conocimientos técnicos sobre maquinaria, adquiridos a la empresa de su padre. En el estudio de Montreuil, demostró que era un mago muy diestro: ideó artefactos, muñecos y engranajes mecánicos prodigiosos que supusieron un salto cualitativo para el mundo del cine.",

    joc_1_es:"¡He aquí la carpeta fantástica del gran mago Jean-Eugène Robert-Houdin, el maestro de Méliès! La carpeta se colocaba encima de un caballete y el mago iba sacando dibujos de papel. Empezaba con la ilustración de una mujer con la cabeza descubierta. Entonces... ¡sorpresa! ¡Salían de la carpeta un par de sombreros! Y, después, salían ramos de flores, ollas, tórtolas vivas... ¡e incluso niños!",
    joc_1_ca:"Vet aquí la carpeta fantàstica del gran mag Jean-Eugène Robert-Houdin, el mestre de Méliès! La carpeta es col·locava damunt d’un cavallet , i el mag anava traient dibuixos de paper. Començava amb la il·lustració d’una dona amb el cap descobert. Llavors...sorpresa! Sortien de la carpeta un parell de barrets! I,després, en sortien rams de flors, olles de coure,tórtores vives i fins i tot nens!",
    joc_2_es:"Este era uno de los grandes números de magia de George Méliès. Surgido de entre el público, un señor barbudo y muy charlatán interrumpía el espectáculo y subía al escenario. El mago, harto de la palabrería, lo decapitaba con un sable y ponía la cabeza en una caja, pero, aun así, ¡el tipo no dejaba de hablar! Tras unos instantes de sainete y persecuciones con el cuerpo decapitado, sacaban un armario de madera trucado (con trampillas y un espejo) para poner la cabeza y que pareciera vacío. A pesar de todos los esfuerzos, ¡no había manera de que el señor se callase!",
    joc_2_ca:"Aquest era un dels grans números de màgia de George Méliès. Sorgit d’entre el públic, un senyor barbut i molt xerraire interrompia l’espectacle i pujava a l’escenari. El mag, tip de la xerrameca, el decapitava amb un sabre i posava el cap en una capsa, però, tot i així, l’home xerra que xerra! Hi havia uns instants de sainet i persecucions amb el cos decapitat. Després, treien un armari de fusta trucat (amb trapes i un mirall) per posar-hi el cap i que semblés buit. I, malgrat tots els esforços, el senyor no callava de cap manera!",
    joc_3_es:"El truco de magia es un medio para engañarnos y dejarnos boquiabiertos. ¿Cómo se puede lograr un engaño visual perfecto? Con un aparato que proyecte imágenes. La linterna mágica era el precedente del cinematógrafo y se puso de moda a mediados del siglo XVII. Méliès la utilizó en su teatro Robert-Houdin. Funcionaba con lentes, transparencias pintadas sobre placas y una lámpara de aceite. El público quedaba aterrorizado con las proyecciones de imágenes macabras de esqueletos, fantasmagorías y espectros. ¡Eran espectáculos terroríficos!",
    joc_3_ca:"El truc de màgia és un mitjà per enganyar-nos i deixar-nos bocabadats. Com es pot aconseguir un engany visual perfecte? Amb un aparell que projecti imatges. La llanterna màgica era el precedent del cinematògraf i es va posar de moda a mitjan segle xvii. Méliès la va utilitzar al seu Teatre Robert-Houdin. Funcionava amb lents,transparències pintades sobre plaques i una làmpada d’oli. El públic quedava esfereït amb les projeccions d’imatges macabres d’esquelets, fantasmagories i espectres. Eren espectacles terrorífics!",
    biografia_titol_es:"Biografía",
    tooltip_cin_es:"El cinematógrafo",
    tooltip_pel_es:"Películas",
    tooltip_tru_es:"Trucajes",
    tooltip_pla_es:"Estudio de Montreuil",

    art_1_es:"Decorados móviles y un pulpo gigante mecanizado en <i>Doscientas millas bajo el mar</i> o <i>La pesadilla de un pescador</i>, 1907",
    art_1_ca:"Decorats mòbils i un pop gegant mecanitzat en <i>Dues-centes milles sota el mar</i> o <i>El malson d'un pescador</i>, 1907",
    art_2_es:"Diorama con efecto de perspectiva en <i>El reino de las hadas</i>, 1903.",
    art_2_ca:"Diorama amb efecte de perspectiva a <i>El regne de les fades</i>, 1903",
    art_3_es:"Tramoya del tren del film <i>El túnel de la Mancha</i> o <i>La pesadilla franco-británica</i>, 1907.",
    art_3_ca:"Tramoia del tren del film <i>El túnel de la Mànega</i> o <i>El malson franco-britànica</i>, 1907.",
    art_4_es:"Dibujo del mecanismo de animación del gigante del Polo utilizado en el film <i>A la conquista del Polo</i>, 1911.",
    art_5_es:"Dibujo del mecanismo de animación del gigante del Polo utilizado en el film <i>A la conquista del Polo</i>, 1911.",
    art_4_ca:"Dibuix del mecanisme d'animació del gegant del Pol utilitzat en el film <i>A la conquesta del Pol</i>, 1911.",
    art_5_ca:"Dibuix del mecanisme d'animació del gegant del Pol utilitzat en el film <i>A la conquesta del Pol</i>, 1911.",
    art_6_es:"Caballo mecánico y carroza en <i>Las cuatrocientas jugarretas del diablo</i>, 1906",
    art_6_ca:"Cavall mecànic i carrossa a <i>Las quatre-centes males passades del diable</i>, 1906"



};
